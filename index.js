function inBangSo() {
  var str = "<table>";
  for (var i = 0; i < 10; i++) {
    str += "<tr>";
    for (var j = 1; j <= 10; j++) {
      str += "<td>" + (i * 10 + j) + "</td>";
    }
    str += "</tr>";
  }
  str += "</table>";
  document.getElementById("result1").innerHTML = str;
}
inBangSo();
//kt số nguyên tố
function isPrimeNumber(number) {
  var isPrimeN = true;
  if (number < 2) return false;
  if (number == 2) return true;
  for (var i = 2; i <= Math.floor(number / 2); i++) {
    if (number % i == 0) {
      return false;
    }
  }
  return isPrimeN;
}
function inPrimeNumber() {
  var myArr = document.getElementById("txt_mang").value.split(",");
  // console.log("myArr: ", myArr);
  var n = myArr.length;
  var str = "Số nguyên tố: ";
  for (var i = 0; i < n; i++) {
    if (isPrimeNumber(myArr[i]) == true) {
      str += myArr[i] + " ";
    }
  }
  document.getElementById("result2").innerText = str;
}

function tinhTong(n) {
  var sum = 0;
  for (var i = 1; i <= n; i++) {
    sum += i + 2;
    console.log("sum: ", sum);
  }
  return sum;
}
function showTong() {
  var nBai3 = document.getElementById("txt_so").value * 1;
  console.log("nBai3: ", nBai3);
  document.getElementById("result3").innerText = ` Tổng = ${tinhTong(nBai3)}`;
}
// bài 4

function timUoc(n, arr) {
  for (var i = 1; i < n; i++) {
    if (n % i == 0) {
      arr.push(i);
    }
  }
}
function showUoc() {
  var arrBai4 = [];
  var n = document.getElementById("txt_SoBai4").value * 1;
  timUoc(n, arrBai4);
  document.getElementById("result4").innerText = ` Ước là: ${arrBai4}`;
  console.log("arrBai4: ", arrBai4);
}
//bài 5
function daoSo() {
  var n = document.getElementById("txt_SoBai5").value;
  var newStr = "";
  for (var i = n.length - 1; i >= 0; i--) {
    newStr += n[i];
  }
  document.getElementById("result5").innerText = newStr;
}
//Bài 6
function findX() {
  var s = 0;
  for (var i = 1; i < 100 / 3; i++) {
    s += i;
    if (s >= 100) {
      document.getElementById("result6").innerText = ` x = ${i}`;
      return;
    }
  }
}
findX();
//Bài 7
function xuatBangCuuChuong() {
  var n = document.getElementById("txt_SoBai7").value * 1;
  var str = "";
  for (var i = 1; i <= 10; i++) {
    str += ` ${n} x ${i} = ${n * i}<br>`;
  }
  document.getElementById("result7").innerHTML = str;
}
//Bìa 8
function chiaBai() {
  var players = [[], [], [], []];
  var cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];

  for (var i = 0; i < cards.length; i++) {
    if (i % 4 == 0) {
      // chia cho người số 1
      players[0].push(cards[i]);
    } else if (i % 4 == 1) {
      // chia cho người số 2
      players[1].push(cards[i]);
    } else if (i % 4 == 2) {
      // chia cho người số 3
      players[2].push(cards[i]);
    } else if (i % 4 == 3) {
      // chia cho người số 4
      players[3].push(cards[i]);
    }
  }
  console.log(players);
  document.getElementById("result8").innerHTML = ` players1 = ${players[0]} <br>
  players2 = ${players[1]} <br>
  players3 = ${players[2]} <br>
  players4 = ${players[3]}`;
}
chiaBai();
//Bai 9
function demSoGaCho() {
  var m = document.getElementById("txt_so_con").value * 1; //số con
  var n = document.getElementById("txt_so_chan").value * 1; //số chân
  var soCho = 0;
  var soGa = 0;
  for (var i = 1; i <= m; i++) {
    soCho = i;
    if (soCho * 4 + (m - soCho) * 2 == n) {
      soGa = m - soCho;
      document.getElementById(
        "result9"
      ).innerText = ` Số chó = ${soCho}, Số gà = ${soGa}`;
      return;
    }
  }
  document.getElementById("result9").innerText = `Nhập lại số chó, số gà.`;
}
//Bài 10
function inDo() {
  var gio = document.getElementById("txt_gio").value * 1;
  var phut = document.getElementById("txt_phut").value * 1;

  var do_gio_phut = Math.abs(gio - Math.floor(phut / 5)) * 30;
  document.getElementById("result10").innerText = do_gio_phut + " độ";
}
